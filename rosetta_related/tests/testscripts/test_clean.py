#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  8 09:50:50 2017

@author: owenwhitley
"""

#Unit_tests

import os
import unittest
import sys

dirName = "amber_rosetta_project"

for root, dir, files, in os.walk("/home/kimlab2/owenwhitley"):
    if dirName in dir:
        proj_path = (os.path.join(root,dirName))
        break

test_path = os.path.join(proj_path,'rosetta_related/tests')
test_output_path = test_path + "/test_output"
ref_data_path = test_path + "/reference_data"
utils_path = os.path.join(test_path,"../rosetta_python_utils")
sys.path.append(utils_path)

import clean_pdb as CL

class test_clean_pdb(unittest.TestCase):
    def setUp(self):
        self.ref_file_obj = open(ref_data_path + "/4b7w_ref_clean.pdb", "r")
        
        CL.clean_pdb(ref_data_path + "/4b7w.pdb", #path to pdb file
                     keepLines = ["ATOM","TER"], #lines in pdb to keep
                     suffix = "_test_clean",
                     parent_dir = test_path, #parent directory of destination folder
                     destPath = "test_output", #name of destination folder
                     overwrite = True, #overwrite existing file?
                     new_directory = False, 
                     logfile = True)
        self.test_file_obj = open(test_path + '/test_output/4b7w_test_clean.pdb', 'r')
        self.ref_pdb_str = self.ref_file_obj.read()
        self.test_pdb_str = self.test_file_obj.read()
        
    def test_expected_outputs(self):
        self.assertEqual(self.ref_pdb_str, self.test_pdb_str)
    
    def tearDown(self):
        
        
        del self.ref_file_obj 
        del self.test_file_obj
        del self.ref_pdb_str
        del self.test_pdb_str
        
if __name__ == '__main__':
    unittest.main()